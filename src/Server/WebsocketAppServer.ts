/// <reference path='../../typings/modules/ws/index.d.ts' />
const ws = require("ws");
import Immutable = require("immutable");
import {ResponseManager} from "./Response/ResponseManager";
import {RequestManager} from "./Request/RequestManager";
import {PluginManager} from "./Plugin/PluginManager";
import {RouteManager} from "./Routing/RouteManager";
import {TransportManager} from "./Transport/TransportManager";

export class WebsocketAppServer {

    private options: WebsocketAppOptions;
    private transportManager: TransportManager = new TransportManager(this);
    private responseManager: ResponseManager = new ResponseManager(this);
    private requestManager: RequestManager = new RequestManager(this);
    private pluginManager: PluginManager = new PluginManager(this);
    private routeManager: RouteManager = new RouteManager(this);

    constructor(options?) {
        this.options = new WebsocketAppOptions(options);
    }

    get Request(): RequestManager {
        return this.requestManager;
    }

    get Response(): ResponseManager {
        return this.responseManager;
    }

    get Plugin(): PluginManager {
        return this.pluginManager;
    }

    get Route(): RouteManager {
        return this.routeManager;
    }

    get Transport(): TransportManager {
        return this.transportManager;
    }

    start() {
        console.log("Starting WebsocketAppServer");
        console.log("Starting Connection and Request Monitoring");
        this.Request.monitorRequestStream();
        console.log("RequestManager Monitoring Request Stream");
        this.Transport.monitorConnectionStream();
        console.log("TransportManager Monitoring Connection Stream");
        this.Transport.monitorRequestStream();
        console.log("TransportManager Monitoring Request Stream");
        console.log("WebsocketAppServer is running");
    }

}

export interface WebsocketAppOptionsAttributes {
    transportType?: string
}

export class WebsocketAppOptions {
    public transportType: string
    public defaults = {
        transportType: "websocket"
    };

    constructor(options?: WebsocketAppOptionsAttributes) {
        this.transportType = this.defaults.transportType;
        if(options){
            if(options.hasOwnProperty("transportType")) {
                this.transportType = options.transportType
            }
        }
    }
}