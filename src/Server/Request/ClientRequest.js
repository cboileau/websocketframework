"use strict";
var ServerResponse_1 = require("../Response/ServerResponse");
var ErrorResponse_1 = require("../Response/ErrorResponse");
var ClientRequest = (function () {
    function ClientRequest(connection) {
        this.connection = connection;
        this.state = ClientRequest.STATE_NEW;
    }
    ClientRequest.prototype.respond = function (responseData) {
        var response = new ServerResponse_1.ServerResponse(this.connection).loadAbstract(responseData);
        response.requestId = this.requestId;
        return response.send();
    };
    ClientRequest.prototype.response = function () {
        var response = new ServerResponse_1.ServerResponse(this.connection);
        response.requestId = this.requestId;
        return response;
    };
    ClientRequest.prototype.unauthorized = function () {
        this.state = ClientRequest.STATE_COMPLETE;
        var response = new ServerResponse_1.ServerResponse(this.connection).loadAbstract(new ErrorResponse_1.ErrorResponse("Unauthorized", 401));
        response.requestId = this.requestId;
        return response.send();
    };
    ClientRequest.prototype.getServiceHandlerConfig = function () {
        return this.connection.server.Route.getServiceHandlerConfig(this.service);
    };
    ClientRequest.prototype.processed = function () {
        this.state = ClientRequest.STATE_PROCESSED;
    };
    ClientRequest.prototype.shouldRoute = function () {
        if (this.state === ClientRequest.STATE_COMPLETE || this.state === ClientRequest.STATE_FAILED) {
            return false;
        }
        return true;
    };
    ClientRequest.STATE_NEW = 0;
    ClientRequest.STATE_PROCESSED = 1;
    ClientRequest.STATE_COMPLETE = 10;
    ClientRequest.STATE_FAILED = 20;
    return ClientRequest;
}());
exports.ClientRequest = ClientRequest;
