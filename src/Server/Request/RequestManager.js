"use strict";
var rxjs_1 = require("@reactivex/rxjs");
var RequestManager = (function () {
    function RequestManager(server) {
        this.server = server;
        this.rawRequestStream = new rxjs_1.Subject();
        this.processedRequestStream = new rxjs_1.Subject();
    }
    RequestManager.prototype.monitorRequestStream = function () {
        var _this = this;
        this.server.Transport.requestStream.subscribe({
            next: function (request) {
                _this.rawRequestStream.next(request);
                _this.processRawRequestHooks(request);
            }
        });
    };
    RequestManager.prototype.processRawRequestHooks = function (request) {
        for (var _i = 0, _a = this.server.Plugin.requestHooks; _i < _a.length; _i++) {
            var requestHook = _a[_i];
            try {
                requestHook.onRequest(request);
            }
            catch (error) {
                console.error("Error processing request hook");
                console.error(error);
            }
        }
        request.processed();
        this.processedRequestStream.next(request);
    };
    return RequestManager;
}());
exports.RequestManager = RequestManager;
