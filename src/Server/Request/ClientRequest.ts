import {ServerResponse, ServerResponseAbstract} from "../Response/ServerResponse";
import {IConnection} from "../Transport/Connection/IConnection";
import {ErrorResponse} from "../Response/ErrorResponse";
import {IServiceHandlerConfig} from "../Services/IServiceHandlerConfig";
export interface ClientRequestInterface{
    service: string,
    payload?: any|null,
    requestId?: string
}

export class ClientRequest {
    public static STATE_NEW = 0;
    public static STATE_PROCESSED = 1;
    public static STATE_COMPLETE = 10;
    public static STATE_FAILED = 20;
    public service: string;
    public payload?: any|null;
    public requestId?: string;
    private state: number = ClientRequest.STATE_NEW;
    constructor(public connection: IConnection) {}

    respond(responseData: ServerResponseAbstract) {
        let response = new ServerResponse(this.connection).loadAbstract(responseData);
        response.requestId = this.requestId;
        return response.send();
    }

    response() {
        let response = new ServerResponse(this.connection);
        response.requestId = this.requestId;
        return response;
    }

    unauthorized() {
        this.state = ClientRequest.STATE_COMPLETE;
        let response = new ServerResponse(this.connection).loadAbstract(new ErrorResponse("Unauthorized", 401));
        response.requestId = this.requestId;
        return response.send();
    }

    getServiceHandlerConfig(): IServiceHandlerConfig {
        return this.connection.server.Route.getServiceHandlerConfig(this.service);
    }

    processed() {
        this.state = ClientRequest.STATE_PROCESSED;
    }

    shouldRoute(): boolean {
        if(this.state === ClientRequest.STATE_COMPLETE || this.state === ClientRequest.STATE_FAILED) {
            return false;
        }
        return true;
    }
}