import {WebsocketAppServer} from "../WebsocketAppServer";
import {Subject} from "@reactivex/rxjs";
import {ClientRequest} from "./ClientRequest";
export class RequestManager {

    rawRequestStream: Subject<ClientRequest> = new Subject<ClientRequest>();
    processedRequestStream: Subject<ClientRequest> = new Subject<ClientRequest>();
    rawRequestHooks: Array<any>;

    constructor(public server: WebsocketAppServer) {}

    monitorRequestStream() {
        this.server.Transport.requestStream.subscribe({
            next: (request: ClientRequest) => {
                this.rawRequestStream.next(request);
                this.processRawRequestHooks(request);
            }
        })
    }

    processRawRequestHooks(request: ClientRequest) {
        for(let requestHook of this.server.Plugin.requestHooks) {
            try {
                requestHook.onRequest(request);
            }
            catch (error) {
                console.error("Error processing request hook");
                console.error(error);
            }
        }
        request.processed();
        this.processedRequestStream.next(request);
    }

}