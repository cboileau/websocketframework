"use strict";
var rxjs_1 = require("@reactivex/rxjs");
var WebsocketTransport_1 = require("./Agents/Websocket/WebsocketTransport");
/**
 * Manages the data transport layer of the server
 */
var TransportManager = (function () {
    function TransportManager(server) {
        this.server = server;
        this.rawConnectionStream = new rxjs_1.Subject();
        this.processedConnectionStream = new rxjs_1.Subject();
        this.requestStream = new rxjs_1.Subject();
        this.setTransportImplementation();
    }
    /**
     * Sets transportInterface to an implementation of ITransport based on server Config
     */
    TransportManager.prototype.setTransportImplementation = function () {
        // TODO read config to switch between implementations
        this.transportInterface = new WebsocketTransport_1.WebsocketTransport(this.server);
    };
    TransportManager.prototype.getAllConnections = function () {
        return this.transportInterface.connections;
    };
    /**
     * Monitors transportInterfaces raw ConnectionInterfaces, updates rawConnectionInterface, and triggers processing raw connection hooks
     */
    TransportManager.prototype.monitorConnectionStream = function () {
        var _this = this;
        this.transportInterface.connectionStream.subscribe({
            next: function (connection) {
                _this.rawConnectionStream.next(connection);
                _this.processRawConnectionHooks(connection);
            }
        });
    };
    /**
     * Monitors transportInterfaces raw ClientRequests and updates requestStream
     */
    TransportManager.prototype.monitorRequestStream = function () {
        var _this = this;
        this.transportInterface.requestStream.subscribe({
            next: function (request) {
                _this.requestStream.next(request);
            }
        });
    };
    /**
     * Processes connections hooks on raw ConnectionInterfaces and updates processConnectionStream
     * @param connection
     */
    TransportManager.prototype.processRawConnectionHooks = function (connection) {
        for (var _i = 0, _a = this.server.Plugin.connectionHooks; _i < _a.length; _i++) {
            var connectionHook = _a[_i];
            try {
                connectionHook.onConnection(connection);
            }
            catch (error) {
                console.error("Error processing connection hook");
                console.error(error);
            }
        }
        this.processedConnectionStream.next(connection);
    };
    return TransportManager;
}());
exports.TransportManager = TransportManager;
