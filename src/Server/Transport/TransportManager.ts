import {WebsocketAppServer} from "../WebsocketAppServer";
import {ITransport} from "./ITransport";
import {IConnection} from "./Connection/IConnection";
import {Subject} from "@reactivex/rxjs";
import {WebsocketTransport} from "./Agents/Websocket/WebsocketTransport";
import {ClientRequest} from "../Request/ClientRequest";
import Immutable = require("immutable");

/**
 * Manages the data transport layer of the server
 */
export class TransportManager {

    public rawConnectionStream: Subject<IConnection> = new Subject<IConnection>();
    public processedConnectionStream: Subject<IConnection> = new Subject<IConnection>();
    public requestStream: Subject<ClientRequest> = new Subject<ClientRequest>();
    private transportInterface: ITransport;

    constructor(private server: WebsocketAppServer) {
        this.setTransportImplementation();
    }

    /**
     * Sets transportInterface to an implementation of ITransport based on server Config
     */
    private setTransportImplementation(){
        // TODO read config to switch between implementations
        this.transportInterface = new WebsocketTransport(this.server);
    }

    getAllConnections(): Immutable.Map<string, IConnection> {
        return this.transportInterface.connections;
    }

    /**
     * Monitors transportInterfaces raw ConnectionInterfaces, updates rawConnectionInterface, and triggers processing raw connection hooks
     */
    monitorConnectionStream() {
        this.transportInterface.connectionStream.subscribe({
            next: (connection: IConnection) => {
                this.rawConnectionStream.next(connection);
                this.processRawConnectionHooks(connection);
            }
        })
    }

    /**
     * Monitors transportInterfaces raw ClientRequests and updates requestStream
     */
    monitorRequestStream() {
        this.transportInterface.requestStream.subscribe({
           next: (request: ClientRequest) => {
               this.requestStream.next(request);
           }
        });
    }

    /**
     * Processes connections hooks on raw ConnectionInterfaces and updates processConnectionStream
     * @param connection
     */
    private processRawConnectionHooks(connection: IConnection) {
        for(let connectionHook of this.server.Plugin.connectionHooks) {
            try {
                connectionHook.onConnection(connection);
            }
            catch(error) {
                console.error("Error processing connection hook");
                console.error(error);
            }
        }
        this.processedConnectionStream.next(connection);
    }

}