import Immutable = require("immutable");
import {WebsocketConnection} from "./WebsocketConnection";
import {ITransport} from "../../ITransport";
import {WebSocketServer} from "../../../../../typings/modules/ws";
import {IConnection} from "../../Connection/IConnection";
import {ServerResponse} from "../../../Response/ServerResponse";
import {WebsocketAppServer} from "../../../WebsocketAppServer";
import {Subject} from "@reactivex/rxjs";
import {ClientRequest} from "../../../Request/ClientRequest";
const ws = require("ws");

export class WebsocketTransport implements ITransport {

    private wss: WebSocketServer;
    public connections: Immutable.Map<string, IConnection> = Immutable.Map<string, WebsocketConnection>();
    public connectionStream: Subject<WebsocketConnection> = new Subject<WebsocketConnection>();
    public requestStream: Subject<ClientRequest> = new Subject<ClientRequest>();

    constructor(private server: WebsocketAppServer) {
        this.wss = new ws.Server({port:3000});
        console.log(this.wss);
        this._bindHandlers();
        this._subscribeEvents();
    }

    async close(): Promise<boolean> {
        try {
            await this.closeServer();
            return true;
        }
        catch (error) {
            console.error(error);
            return false;
        }
    }

    async send(response: ServerResponse): Promise<any> {
        try {

        }
        catch (error) {

        }
    }

    private _subscribeEvents() {
        this.monitorConnections();
    }

    private monitorConnections() {
        this.connectionStream.subscribe({
            next: (connection: WebsocketConnection) => {
                connection.requestStream.subscribe({
                    next: (clientRequest: ClientRequest) => {
                        this.requestStream.next(clientRequest);
                    }
                })
                connection.status.subscribe({
                    complete: () => {
                        this.connections.delete(connection.uuid);
                    }
                })
            }
        });
    }

    private _bindHandlers() {
        console.log("Binding Websocket Server event handlers")
        const WebsocketTransport = this;
        this.wss.on("connection", (ws) => {
            WebsocketTransport.onConnection(ws);
        });
    }

    onConnection(clientConnection) {
        let connection = new WebsocketConnection(this.server, clientConnection);
        console.log("New client connected ("+connection.uuid+")");
        this.connections.set(connection.uuid, connection);
        this.connectionStream.next(connection);
    }

    closeServer(): Promise<boolean> {
        return new Promise<boolean> ((resolve, reject) => {
            try{
                this.wss.close();
                resolve(true);
            }
            catch(error){
                reject(error);
            }
        });
    }
}