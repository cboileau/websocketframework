"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
var Immutable = require("immutable");
var WebsocketConnection_1 = require("./WebsocketConnection");
var rxjs_1 = require("@reactivex/rxjs");
var ws = require("ws");
var WebsocketTransport = (function () {
    function WebsocketTransport(server) {
        this.server = server;
        this.connections = Immutable.Map();
        this.connectionStream = new rxjs_1.Subject();
        this.requestStream = new rxjs_1.Subject();
        this.wss = new ws.Server({ port: 3000 });
        console.log(this.wss);
        this._bindHandlers();
        this._subscribeEvents();
    }
    WebsocketTransport.prototype.close = function () {
        return __awaiter(this, void 0, Promise, function* () {
            try {
                yield this.closeServer();
                return true;
            }
            catch (error) {
                console.error(error);
                return false;
            }
        });
    };
    WebsocketTransport.prototype.send = function (response) {
        return __awaiter(this, void 0, Promise, function* () {
            try {
            }
            catch (error) {
            }
        });
    };
    WebsocketTransport.prototype._subscribeEvents = function () {
        this.monitorConnections();
    };
    WebsocketTransport.prototype.monitorConnections = function () {
        var _this = this;
        this.connectionStream.subscribe({
            next: function (connection) {
                connection.requestStream.subscribe({
                    next: function (clientRequest) {
                        _this.requestStream.next(clientRequest);
                    }
                });
                connection.status.subscribe({
                    complete: function () {
                        _this.connections.delete(connection.uuid);
                    }
                });
            }
        });
    };
    WebsocketTransport.prototype._bindHandlers = function () {
        console.log("Binding Websocket Server event handlers");
        var WebsocketTransport = this;
        this.wss.on("connection", function (ws) {
            WebsocketTransport.onConnection(ws);
        });
    };
    WebsocketTransport.prototype.onConnection = function (clientConnection) {
        var connection = new WebsocketConnection_1.WebsocketConnection(this.server, clientConnection);
        console.log("New client connected (" + connection.uuid + ")");
        this.connections.set(connection.uuid, connection);
        this.connectionStream.next(connection);
    };
    WebsocketTransport.prototype.closeServer = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                _this.wss.close();
                resolve(true);
            }
            catch (error) {
                reject(error);
            }
        });
    };
    return WebsocketTransport;
}());
exports.WebsocketTransport = WebsocketTransport;
