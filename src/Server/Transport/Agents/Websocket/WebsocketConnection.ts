import {IConnection} from "../../Connection/IConnection";
import {ClientRequest} from "../../../Request/ClientRequest";
import {WebsocketAppServer} from "../../../WebsocketAppServer";
import {ServerResponse} from "../../../Response/ServerResponse";
import {Subject} from "@reactivex/rxjs";
import {ServerBroadcast} from "../../../Response/ServerBroadcast";
const uuid = require("node-uuid");

export class WebsocketConnection implements IConnection {

    public uuid: string;
    public requestStream: Subject<ClientRequest> = new Subject<ClientRequest>();
    public status: Subject<string> = new Subject<string>();

    constructor(public server: WebsocketAppServer, private websocket) {
        this.uuid = uuid.v4();
        const connection = this;
        this.websocket.on("message", (requestString) => {
            connection.onRequest(requestString);
        });
        this.websocket.on("close", () => {
            connection.onClose();
        });
        this.status.next("open");
    }

    onClose() {
        this.status.next("closing");
        this.status.next("closed");
        this.status.complete();
    }

    onRequest(requestString: string) {
        const requestObj = JSON.parse(requestString);
        const clientRequest = new ClientRequest(this);
        clientRequest.connection = this;
        clientRequest.payload = requestObj.payload;
        clientRequest.requestId = requestObj.requestId;
        clientRequest.service = requestObj.service;
        this.requestStream.next(clientRequest);
    }

    sendResponse(serverResponse: ServerResponse) {
        this.websocket.send(serverResponse.toJSON());
    }

    sendBroadcast(serverBroadcast: ServerBroadcast) {
        this.websocket.send(serverBroadcast);
    }



}