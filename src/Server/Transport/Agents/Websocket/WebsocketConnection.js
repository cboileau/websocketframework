"use strict";
var ClientRequest_1 = require("../../../Request/ClientRequest");
var rxjs_1 = require("@reactivex/rxjs");
var uuid = require("node-uuid");
var WebsocketConnection = (function () {
    function WebsocketConnection(server, websocket) {
        this.server = server;
        this.websocket = websocket;
        this.requestStream = new rxjs_1.Subject();
        this.status = new rxjs_1.Subject();
        this.uuid = uuid.v4();
        var connection = this;
        this.websocket.on("message", function (requestString) {
            connection.onRequest(requestString);
        });
        this.websocket.on("close", function () {
            connection.onClose();
        });
        this.status.next("open");
    }
    WebsocketConnection.prototype.onClose = function () {
        this.status.next("closing");
        this.status.next("closed");
        this.status.complete();
    };
    WebsocketConnection.prototype.onRequest = function (requestString) {
        var requestObj = JSON.parse(requestString);
        var clientRequest = new ClientRequest_1.ClientRequest(this);
        clientRequest.connection = this;
        clientRequest.payload = requestObj.payload;
        clientRequest.requestId = requestObj.requestId;
        clientRequest.service = requestObj.service;
        this.requestStream.next(clientRequest);
    };
    WebsocketConnection.prototype.sendResponse = function (serverResponse) {
        this.websocket.send(serverResponse.toJSON());
    };
    WebsocketConnection.prototype.sendBroadcast = function (serverBroadcast) {
        this.websocket.send(serverBroadcast);
    };
    return WebsocketConnection;
}());
exports.WebsocketConnection = WebsocketConnection;
