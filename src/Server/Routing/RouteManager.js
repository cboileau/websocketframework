"use strict";
/**
 * Manages service routes and routes ClientRequests to service handlers
 */
var RouteManager = (function () {
    function RouteManager(server) {
        this.server = server;
        this.routes = new Map();
        this.monitorProcessedRequestStream();
    }
    /**
     * Registers and array of route configrations
     * @param routeConfigs
     */
    RouteManager.prototype.registerRoutes = function (routeConfigs) {
        for (var _i = 0, routeConfigs_1 = routeConfigs; _i < routeConfigs_1.length; _i++) {
            var routeConfig = routeConfigs_1[_i];
            this.register(routeConfig);
        }
    };
    RouteManager.prototype.getRouteConfig = function (path) {
        if (this.routes.has(path)) {
            return this.routes.get(path);
        }
        console.error("Route with path <" + path + "> is not registered");
    };
    RouteManager.prototype.getServiceHandlerConfig = function (path) {
        if (this.routes.has(path)) {
            return this.routes.get(path).handler;
        }
        console.error("Route with path <" + path + "> is not registered");
    };
    /**
     * Monitors processedRequestStream and triggers routing the request
     */
    RouteManager.prototype.monitorProcessedRequestStream = function () {
        var _this = this;
        this.server.Request.processedRequestStream.subscribe({
            next: function (clientRequest) {
                _this.routeRequest(clientRequest);
            }
        });
    };
    /**
     * Routes a ClientRequest to route handler with matching service path
     * @param request
     * @returns {ServerResponse}
     */
    RouteManager.prototype.routeRequest = function (request) {
        if (this.routes.has(request.service)) {
            if (request.shouldRoute()) {
                console.log("Routing Request: " + request.service);
                this.routes.get(request.service).handler.handler(request);
            }
        }
        else {
            console.error("Cannot route request. Route with path <" + request.service + "> is not registered");
        }
    };
    /**
     * Registers a route configration
     * @param routeConfig
     */
    RouteManager.prototype.register = function (routeConfig) {
        if (this.routes.has(routeConfig.path)) {
            throw new Error("Service path: <" + routeConfig.path + "> has already been registered");
        }
        this.routes.set(routeConfig.path, routeConfig);
    };
    return RouteManager;
}());
exports.RouteManager = RouteManager;
