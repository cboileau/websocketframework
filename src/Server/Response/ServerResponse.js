"use strict";
var ServerResponseAbstract = (function () {
    function ServerResponseAbstract() {
        this.statusCode = 200;
    }
    ServerResponseAbstract.prototype.status = function (statusCode) {
        this.statusCode = statusCode;
    };
    ServerResponseAbstract.prototype.get = function (key) {
        if (this.hasOwnProperty(key)) {
            return this[key];
        }
        return undefined;
    };
    ServerResponseAbstract.prototype.has = function (key) {
        return this.hasOwnProperty(key);
    };
    return ServerResponseAbstract;
}());
exports.ServerResponseAbstract = ServerResponseAbstract;
var ServerResponse = (function () {
    function ServerResponse(connection) {
        this.connection = connection;
        this.statusCode = 200;
        this.server = this.connection.server;
    }
    ServerResponse.prototype.loadAbstract = function (abstract) {
        if (abstract.has("statusCode"))
            this.statusCode = abstract.get("statusCode");
        if (abstract.has("payload"))
            this.payload = abstract.get("payload");
        if (abstract.has("payloadClass"))
            this.payloadClass = abstract.get("payloadClass");
        if (abstract.has("event"))
            this.event = abstract.get("event");
        return this;
    };
    ServerResponse.prototype.loadAttributes = function (responseAttributes) {
        this.set(responseAttributes, [
            "payload",
            "payloadClass",
            "event",
            "statusCode"
        ]);
    };
    ServerResponse.prototype.set = function (attributes, props) {
        for (var _i = 0, props_1 = props; _i < props_1.length; _i++) {
            var prop = props_1[_i];
            if (attributes.hasOwnProperty(prop)) {
                this[prop] = attributes[prop];
            }
        }
    };
    ServerResponse.prototype.toObject = function () {
        return {
            requestId: this.requestId,
            statusCode: this.statusCode,
            payload: this.payload,
            payloadClass: this.payloadClass,
            event: this.event
        };
    };
    ServerResponse.prototype.toJSON = function () {
        return JSON.stringify(this.toObject());
    };
    ServerResponse.prototype.send = function () {
        try {
            console.log("Responding to client: ");
            console.log(this.toObject());
            this.connection.sendResponse(this);
        }
        catch (error) {
            console.error(error);
        }
        return this;
    };
    ServerResponse.prototype.to = function (requestId) {
        this.requestId = requestId;
        return this;
    };
    ServerResponse.prototype.status = function (statusCode) {
        this.statusCode = statusCode;
        return this;
    };
    ServerResponse.prototype.with = function (payload) {
        this.payload = payload;
        return this;
    };
    ServerResponse.prototype.asClass = function (payloadClass) {
        this.payloadClass = payloadClass;
        return this;
    };
    ServerResponse.prototype.triggering = function (event) {
        this.event = event;
        return this;
    };
    return ServerResponse;
}());
exports.ServerResponse = ServerResponse;
