import {ServerResponseAbstract} from "./ServerResponse";
export class ErrorResponse extends ServerResponseAbstract {

    constructor(message: string, statusCode: number) {
        super();
        this.payload = {
            errorMessage: message
        }
        this.statusCode = statusCode;
    }

}