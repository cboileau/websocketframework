"use strict";
var _ = require("lodash");
var ServerBroadcast_1 = require("./ServerBroadcast");
var Immutable = require("immutable");
var ResponseManager = (function () {
    function ResponseManager(server) {
        this.server = server;
        this.responses = Immutable.Map();
    }
    ResponseManager.prototype.createBroadcast = function () {
        return new ServerBroadcast_1.ServerBroadcast(this.server);
    };
    ResponseManager.prototype.register = function (key, serverResponse) {
        if (this.responses.has(key)) {
            throw new Error("A response with the name <" + key + "> has already been registered");
        }
        else {
            this.responses.set(key, serverResponse);
        }
    };
    /**
     * Returns a clone of the response type
     */
    ResponseManager.prototype.get = function (key) {
        if (this.responses.has(key)) {
            return _.clone(this.responses.get(key));
        }
    };
    return ResponseManager;
}());
exports.ResponseManager = ResponseManager;
