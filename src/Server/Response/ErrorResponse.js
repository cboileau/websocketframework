"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ServerResponse_1 = require("./ServerResponse");
var ErrorResponse = (function (_super) {
    __extends(ErrorResponse, _super);
    function ErrorResponse(message, statusCode) {
        _super.call(this);
        this.payload = {
            errorMessage: message
        };
        this.statusCode = statusCode;
    }
    return ErrorResponse;
}(ServerResponse_1.ServerResponseAbstract));
exports.ErrorResponse = ErrorResponse;
