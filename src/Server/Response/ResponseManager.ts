import {ServerResponseInterface} from "./ServerResponse";
import * as _ from "lodash";
import {WebsocketAppServer} from "../WebsocketAppServer";
import {ServerBroadcast} from "./ServerBroadcast";
import Immutable = require("immutable");

export class ResponseManager {

    private responses: Immutable.Map<string, ServerResponseInterface> = Immutable.Map<string, ServerResponseInterface>()

    constructor(public server: WebsocketAppServer) {

    }

    createBroadcast() {
        return new ServerBroadcast(this.server);
    }

    register(key, serverResponse: ServerResponseInterface) {
        if(this.responses.has(key)){
            throw new Error("A response with the name <"+key+"> has already been registered");
        }
        else{
            this.responses.set(key, serverResponse);
        }
    }

    /**
     * Returns a clone of the response type
     */
    get(key) {
        if(this.responses.has(key)){
            return _.clone(this.responses.get(key))
        }

    }

}