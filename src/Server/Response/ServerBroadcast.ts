import {WebsocketAppServer} from "../WebsocketAppServer";
import {IConnection} from "../Transport/Connection/IConnection";

export class ServerBroadcast {
    public statusCode: number = 200;
    public payload: any;
    public payloadClass: string|null;
    public event: string;
    private _connections: Array<IConnection> = [];

    connections(connections: Array<IConnection>) {
        this._connections = connections;
    }

    sendAll() {
        let connections: Array<IConnection> = this.server.Transport.getAllConnections().toArray();
        this.connections(connections);
        this.send();
    }

    send() {
        for(let connection of this._connections) {
            connection.sendBroadcast(this);
        }
    }

    constructor(public server: WebsocketAppServer) {}
}