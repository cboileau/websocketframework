"use strict";
var ServerBroadcast = (function () {
    function ServerBroadcast(server) {
        this.server = server;
        this.statusCode = 200;
        this._connections = [];
    }
    ServerBroadcast.prototype.connections = function (connections) {
        this._connections = connections;
    };
    ServerBroadcast.prototype.sendAll = function () {
        var connections = this.server.Transport.getAllConnections().toArray();
        this.connections(connections);
        this.send();
    };
    ServerBroadcast.prototype.send = function () {
        for (var _i = 0, _a = this._connections; _i < _a.length; _i++) {
            var connection = _a[_i];
            connection.sendBroadcast(this);
        }
    };
    return ServerBroadcast;
}());
exports.ServerBroadcast = ServerBroadcast;
