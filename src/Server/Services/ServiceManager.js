"use strict";
var ServiceManager = (function () {
    function ServiceManager(server) {
        this.server = server;
        this.services = new Map();
    }
    ServiceManager.prototype.register = function (path, endpointConfig) {
        if (this.services.has(endpointConfig.path)) {
            throw new Error("Service path: <" + endpointConfig.path + "> has already been registered");
        }
        this.services.set(endpointConfig.path, endpointConfig);
    };
    return ServiceManager;
}());
exports.ServiceManager = ServiceManager;
