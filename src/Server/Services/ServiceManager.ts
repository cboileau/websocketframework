import {WebsocketAppServer} from "../WebsocketAppServer";
import {IRouteConfig} from "./IRouteConfig";
export class ServiceManager {

    private services: Map<string, IRouteConfig> = new Map();

    constructor(public server: WebsocketAppServer) {}

    register(path: string, endpointConfig: IRouteConfig) {
        if(this.services.has(endpointConfig.path)) {
            throw new Error("Service path: <"+endpointConfig.path+"> has already been registered")
        }
        this.services.set(endpointConfig.path, endpointConfig);
    }

}