import {IServiceHandlerConfig} from "./IServiceHandlerConfig";

export interface IRouteConfig {
    path: string;
    handler: IServiceHandlerConfig;
}