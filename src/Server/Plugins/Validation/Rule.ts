import {StringValidator} from "./Validators/StringValidator";
export class Rule {

    private validations = []

    string() {
        this.validations.push(new StringValidator());
    }

    validate(): boolean {
        for(let validation of this.validations) {
            const result = validation.validate();
            if (!result.passed()){
                console.error(result.error())
                return false;
            }
        }
        return true;
    }


}

export class RuleResult {
    private errorMessage: string;

    constructor(private success: boolean, errorMessage: string = ""){
        this.success = success;
    }

    passed() {
        return this.success;
    }

    error() {
        return this.errorMessage;
    }

}

export interface ValidatorInterface {
    validate: (input) => RuleResult;
}