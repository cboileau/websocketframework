import {ValidatorInterface, RuleResult} from "../Rule";

export class StringValidator implements ValidatorInterface {

    validate(input) {
        if(typeof input == "string") {
            return new RuleResult(true);
        }
        else{
            return new RuleResult(false, "Input is not a string");
        }
    }

}