"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var WebsocketAppPlugin_1 = require("../../Plugin/WebsocketAppPlugin");
var Validation_1 = require("./Validation");
var ValidationPlugin = (function (_super) {
    __extends(ValidationPlugin, _super);
    function ValidationPlugin() {
        _super.apply(this, arguments);
        this.pluginName = "WSA.ValidatePayload";
    }
    ValidationPlugin.prototype.registerPlugin = function (server) {
        var payloadValidation = new Validation_1.PayloadValidation();
    };
    return ValidationPlugin;
}(WebsocketAppPlugin_1.WebsocketAppPlugin));
exports.ValidationPlugin = ValidationPlugin;
