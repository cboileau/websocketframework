"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var WebsocketAppPlugin_1 = require("../../Plugin/WebsocketAppPlugin");
var WebsocketAppAuthPlugin = (function (_super) {
    __extends(WebsocketAppAuthPlugin, _super);
    function WebsocketAppAuthPlugin() {
        _super.apply(this, arguments);
        this.pluginName = "WSA.Auth";
    }
    WebsocketAppAuthPlugin.prototype.registerPlugin = function (server) {
        console.log("Registing Auth Plugin");
        server.Plugin.registerConnectionHook(this);
        server.Plugin.registerRawRequestHook(this);
    };
    WebsocketAppAuthPlugin.prototype.onConnection = function (connection) {
        console.log("AuthPlugin OnConnection Hook");
        connection.auth = true;
    };
    WebsocketAppAuthPlugin.prototype.onRequest = function (request) {
        if (request.getServiceHandlerConfig().auth === true) {
            if (request.connection.auth !== true) {
                console.log("Unauthorized Access");
                request.unauthorized();
            }
        }
    };
    return WebsocketAppAuthPlugin;
}(WebsocketAppPlugin_1.WebsocketAppPlugin));
exports.WebsocketAppAuthPlugin = WebsocketAppAuthPlugin;
