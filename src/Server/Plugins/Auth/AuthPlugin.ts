import {WebsocketAppPlugin} from "../../Plugin/WebsocketAppPlugin";
import {WebsocketAppServer} from "../../WebsocketAppServer";
import {IConnection} from "../../Transport/Connection/IConnection";
import {ClientRequest} from "../../Request/ClientRequest";

export class WebsocketAppAuthPlugin extends WebsocketAppPlugin{

    public pluginName = "WSA.Auth";

    registerPlugin(server: WebsocketAppServer) {
        console.log("Registing Auth Plugin");
        server.Plugin.registerConnectionHook(this);
        server.Plugin.registerRawRequestHook(this);
    }

    onConnection(connection: IConnection) {
        console.log("AuthPlugin OnConnection Hook");
        connection.auth = true;
    }

    onRequest(request: ClientRequest) {
        if(request.getServiceHandlerConfig().auth === true) {
            if(request.connection.auth !== true) {
                console.log("Unauthorized Access");
                request.unauthorized();
            }
        }
    }

}