"use strict";
/// <reference path='../../typings/modules/ws/index.d.ts' />
var ws = require("ws");
var ResponseManager_1 = require("./Response/ResponseManager");
var RequestManager_1 = require("./Request/RequestManager");
var PluginManager_1 = require("./Plugin/PluginManager");
var RouteManager_1 = require("./Routing/RouteManager");
var TransportManager_1 = require("./Transport/TransportManager");
var WebsocketAppServer = (function () {
    function WebsocketAppServer(options) {
        this.transportManager = new TransportManager_1.TransportManager(this);
        this.responseManager = new ResponseManager_1.ResponseManager(this);
        this.requestManager = new RequestManager_1.RequestManager(this);
        this.pluginManager = new PluginManager_1.PluginManager(this);
        this.routeManager = new RouteManager_1.RouteManager(this);
        this.options = new WebsocketAppOptions(options);
    }
    Object.defineProperty(WebsocketAppServer.prototype, "Request", {
        get: function () {
            return this.requestManager;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsocketAppServer.prototype, "Response", {
        get: function () {
            return this.responseManager;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsocketAppServer.prototype, "Plugin", {
        get: function () {
            return this.pluginManager;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsocketAppServer.prototype, "Route", {
        get: function () {
            return this.routeManager;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsocketAppServer.prototype, "Transport", {
        get: function () {
            return this.transportManager;
        },
        enumerable: true,
        configurable: true
    });
    WebsocketAppServer.prototype.start = function () {
        console.log("Starting WebsocketAppServer");
        console.log("Starting Connection and Request Monitoring");
        this.Request.monitorRequestStream();
        console.log("RequestManager Monitoring Request Stream");
        this.Transport.monitorConnectionStream();
        console.log("TransportManager Monitoring Connection Stream");
        this.Transport.monitorRequestStream();
        console.log("TransportManager Monitoring Request Stream");
        console.log("WebsocketAppServer is running");
    };
    return WebsocketAppServer;
}());
exports.WebsocketAppServer = WebsocketAppServer;
var WebsocketAppOptions = (function () {
    function WebsocketAppOptions(options) {
        this.defaults = {
            transportType: "websocket"
        };
        this.transportType = this.defaults.transportType;
        if (options) {
            if (options.hasOwnProperty("transportType")) {
                this.transportType = options.transportType;
            }
        }
    }
    return WebsocketAppOptions;
}());
exports.WebsocketAppOptions = WebsocketAppOptions;
