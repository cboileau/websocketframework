import {WebsocketAppServer} from "../WebsocketAppServer";
import {WebsocketAppPlugin} from "./WebsocketAppPlugin";
export class PluginManager {

    requestHooks: Array<WebsocketAppPlugin> = [];
    connectionHooks: Array<WebsocketAppPlugin> = []
    plugins: Map<string, WebsocketAppPlugin> = new Map<string, WebsocketAppPlugin>();

    constructor(private server: WebsocketAppServer) {

    }

    register(plugin: WebsocketAppPlugin) {
        try {
            plugin.registerPlugin(this.server);
            this.plugins.set(plugin.getName(), plugin);
            console.log("Registered Plugin: "+plugin.getName());
        }
        catch (error) {
            console.error("Error registering plugin");
            console.error(error);
        }
    }


    registerRawRequestHook(plugin: WebsocketAppPlugin) {
        this.requestHooks.push(plugin);
    }


    registerConnectionHook(plugin: WebsocketAppPlugin) {
        this.connectionHooks.push(plugin);
    }


}