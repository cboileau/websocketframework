import {WebsocketAppServer} from "../WebsocketAppServer";
import {IConnection} from "../Transport/Connection/IConnection";
import {ClientRequest} from "../Request/ClientRequest";
export abstract class WebsocketAppPlugin {

    abstract pluginName: string;
    abstract registerPlugin(server: WebsocketAppServer): void
    getName(): string {
        return this.pluginName;
    }
    onConnection(connection: IConnection): void {}
    onRequest(request: ClientRequest): void {}


}