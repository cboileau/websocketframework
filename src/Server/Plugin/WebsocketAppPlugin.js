"use strict";
var WebsocketAppPlugin = (function () {
    function WebsocketAppPlugin() {
    }
    WebsocketAppPlugin.prototype.getName = function () {
        return this.pluginName;
    };
    WebsocketAppPlugin.prototype.onConnection = function (connection) { };
    WebsocketAppPlugin.prototype.onRequest = function (request) { };
    return WebsocketAppPlugin;
}());
exports.WebsocketAppPlugin = WebsocketAppPlugin;
