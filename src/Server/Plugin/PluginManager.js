"use strict";
var PluginManager = (function () {
    function PluginManager(server) {
        this.server = server;
        this.requestHooks = [];
        this.connectionHooks = [];
        this.plugins = new Map();
    }
    PluginManager.prototype.register = function (plugin) {
        try {
            plugin.registerPlugin(this.server);
            this.plugins.set(plugin.getName(), plugin);
            console.log("Registered Plugin: " + plugin.getName());
        }
        catch (error) {
            console.error("Error registering plugin");
            console.error(error);
        }
    };
    PluginManager.prototype.registerRawRequestHook = function (plugin) {
        this.requestHooks.push(plugin);
    };
    PluginManager.prototype.registerConnectionHook = function (plugin) {
        this.connectionHooks.push(plugin);
    };
    return PluginManager;
}());
exports.PluginManager = PluginManager;
