import {ServerResponseAbstract} from "../../../Server/Response/ServerResponse";

export class AuthResponse extends ServerResponseAbstract {

    public payload = {
        authSuccess: null,
        userId: undefined
    };

    valid(userId: number) {
        this.statusCode = 200;
        this.payload.userId = userId;
        this.payload.authSuccess = true;
        return this;
    }

    invalid() {
        this.payload.authSuccess = false;
        return this;
    }

}