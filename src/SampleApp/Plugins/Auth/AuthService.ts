import {ClientRequest} from "../../../Server/Request/ClientRequest";
import {AuthResponse} from "./AuthResponses";

export class AuthService {
    serviceEndpoint() {
        return {
            handler: (request: ClientRequest) => {
                console.log("SERVICE ENDPOINT REACHED");
                if(request.payload.email === "craigboileau@gmail.com" && request.payload.password === "C1r9a8i7g") {
                    request.respond(new AuthResponse().valid(1));
                }
                else{
                    request.respond(new AuthResponse().invalid());
                }
            }
        }
    }
}