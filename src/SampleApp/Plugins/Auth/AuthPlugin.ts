import {WebsocketAppPlugin} from "../../../Server/Plugin/WebsocketAppPlugin";
import {WebsocketAppServer} from "../../../Server/WebsocketAppServer";
import {AuthService} from "./AuthService";

export class AuthPlugin extends WebsocketAppPlugin {

    public pluginName = "AuthPlugin";

    registerPlugin(server: WebsocketAppServer) {
        let authService = new AuthService();
        server.Route.registerRoutes([
            { path: "user/auth", handler: authService.serviceEndpoint()},
        ]);
    }

}
