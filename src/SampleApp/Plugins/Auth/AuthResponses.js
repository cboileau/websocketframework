"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ServerResponse_1 = require("../../../Server/Response/ServerResponse");
var AuthResponse = (function (_super) {
    __extends(AuthResponse, _super);
    function AuthResponse() {
        _super.apply(this, arguments);
        this.payload = {
            authSuccess: null,
            userId: undefined
        };
    }
    AuthResponse.prototype.valid = function (userId) {
        this.statusCode = 200;
        this.payload.userId = userId;
        this.payload.authSuccess = true;
        return this;
    };
    AuthResponse.prototype.invalid = function () {
        this.payload.authSuccess = false;
        return this;
    };
    return AuthResponse;
}(ServerResponse_1.ServerResponseAbstract));
exports.AuthResponse = AuthResponse;
