"use strict";
var AuthResponses_1 = require("./AuthResponses");
var AuthService = (function () {
    function AuthService() {
    }
    AuthService.prototype.serviceEndpoint = function () {
        return {
            handler: function (request) {
                console.log("SERVICE ENDPOINT REACHED");
                if (request.payload.email === "craigboileau@gmail.com" && request.payload.password === "C1r9a8i7g") {
                    request.respond(new AuthResponses_1.AuthResponse().valid(1));
                }
                else {
                    request.respond(new AuthResponses_1.AuthResponse().invalid());
                }
            }
        };
    };
    return AuthService;
}());
exports.AuthService = AuthService;
