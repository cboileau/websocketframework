"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var WebsocketAppPlugin_1 = require("../../../Server/Plugin/WebsocketAppPlugin");
var AuthService_1 = require("./AuthService");
var AuthPlugin = (function (_super) {
    __extends(AuthPlugin, _super);
    function AuthPlugin() {
        _super.apply(this, arguments);
        this.pluginName = "AuthPlugin";
    }
    AuthPlugin.prototype.registerPlugin = function (server) {
        var authService = new AuthService_1.AuthService();
        server.Route.registerRoutes([
            { path: "user/auth", handler: authService.serviceEndpoint() },
        ]);
    };
    return AuthPlugin;
}(WebsocketAppPlugin_1.WebsocketAppPlugin));
exports.AuthPlugin = AuthPlugin;
