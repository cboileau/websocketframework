"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var WebsocketAppPlugin_1 = require("../../../Server/Plugin/WebsocketAppPlugin");
var ChatService_1 = require("./ChatService");
var ChatPlugin = (function (_super) {
    __extends(ChatPlugin, _super);
    function ChatPlugin() {
        _super.apply(this, arguments);
        this.pluginName = "ChatPlugin";
    }
    ChatPlugin.prototype.registerPlugin = function (server) {
        var chatService = new ChatService_1.ChatService();
        server.Route.registerRoutes([
            { path: "chat/join", handler: chatService.joinChat() },
            { path: "chat/addMessage", handler: chatService.addMessage() }
        ]);
    };
    return ChatPlugin;
}(WebsocketAppPlugin_1.WebsocketAppPlugin));
exports.ChatPlugin = ChatPlugin;
