import {ReplaySubject} from "@reactivex/rxjs";
import {ClientRequest} from "../../../Server/Request/ClientRequest";
import {ChatMessageResponse, ChatMessageSentResponse} from "./ChatResponses";

export class Message {
    constructor(
        public username: string,
        public message: string
    ) {}
}

export class ChatService {
    messages: ReplaySubject<Message> = new ReplaySubject<Message>(30);
    joinChat() {
        return {
            auth: true,
            handler: (request: ClientRequest) => {
                this.messages.next(new Message(request.connection.uuid, "has joined chat!"))
                this.messages.subscribe({
                    next: (message) => {
                        request.respond(new ChatMessageResponse().message(message));
                    }
                });
            }
        }
    }
    addMessage() {
        return {
            auth: true,
            handler: (request: ClientRequest) => {
                console.log()
                this.messages.next(new Message(request.connection.uuid, request.payload.message))
                request.respond(new ChatMessageSentResponse());
            }
        }
    }
}