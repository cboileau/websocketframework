"use strict";
var rxjs_1 = require("@reactivex/rxjs");
var ChatResponses_1 = require("./ChatResponses");
var Message = (function () {
    function Message(username, message) {
        this.username = username;
        this.message = message;
    }
    return Message;
}());
exports.Message = Message;
var ChatService = (function () {
    function ChatService() {
        this.messages = new rxjs_1.ReplaySubject(30);
    }
    ChatService.prototype.joinChat = function () {
        var _this = this;
        return {
            auth: true,
            handler: function (request) {
                _this.messages.next(new Message(request.connection.uuid, "has joined chat!"));
                _this.messages.subscribe({
                    next: function (message) {
                        request.respond(new ChatResponses_1.ChatMessageResponse().message(message));
                    }
                });
            }
        };
    };
    ChatService.prototype.addMessage = function () {
        var _this = this;
        return {
            auth: true,
            handler: function (request) {
                console.log();
                _this.messages.next(new Message(request.connection.uuid, request.payload.message));
                request.respond(new ChatResponses_1.ChatMessageSentResponse());
            }
        };
    };
    return ChatService;
}());
exports.ChatService = ChatService;
