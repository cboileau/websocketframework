import {WebsocketAppPlugin} from "../../../Server/Plugin/WebsocketAppPlugin";
import {WebsocketAppServer} from "../../../Server/WebsocketAppServer";
import {ChatService} from "./ChatService";

export class ChatPlugin extends WebsocketAppPlugin {

    public pluginName = "ChatPlugin";

    registerPlugin(server: WebsocketAppServer) {
        let chatService = new ChatService();
        server.Route.registerRoutes([
            { path: "chat/join", handler: chatService.joinChat()},
            { path: "chat/addMessage", handler: chatService.addMessage()}
        ]);
    }

}
