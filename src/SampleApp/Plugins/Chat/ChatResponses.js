"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ServerResponse_1 = require("../../../Server/Response/ServerResponse");
var ChatMessageResponse = (function (_super) {
    __extends(ChatMessageResponse, _super);
    function ChatMessageResponse() {
        _super.apply(this, arguments);
        this.payload = { message: undefined };
    }
    ChatMessageResponse.prototype.message = function (message) {
        this.payload.message = message;
        return this;
    };
    return ChatMessageResponse;
}(ServerResponse_1.ServerResponseAbstract));
exports.ChatMessageResponse = ChatMessageResponse;
var ChatMessageSentResponse = (function (_super) {
    __extends(ChatMessageSentResponse, _super);
    function ChatMessageSentResponse() {
        _super.apply(this, arguments);
        this.statusCode = 200;
    }
    return ChatMessageSentResponse;
}(ServerResponse_1.ServerResponseAbstract));
exports.ChatMessageSentResponse = ChatMessageSentResponse;
