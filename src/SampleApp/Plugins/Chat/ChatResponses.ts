import {ServerResponseAbstract} from "../../../Server/Response/ServerResponse";
import {Message} from "./ChatService";

export class ChatMessageResponse extends ServerResponseAbstract {

    payload: {
        message: Message
    } = { message: undefined }

    message(message: Message) {
        this.payload.message = message;
        return this;
    }

}

export class ChatMessageSentResponse extends ServerResponseAbstract {

    payload: undefined
    statusCode = 200;


}