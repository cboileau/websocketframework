import {WebsocketAppServer} from "../Server/WebsocketAppServer";
import {AuthPlugin} from "./Plugins/Auth/AuthPlugin";
import {WebsocketAppAuthPlugin} from "../Server/Plugins/Auth/AuthPlugin";
import {ChatPlugin} from "./Plugins/Chat/ChatPlugin";
const app = new WebsocketAppServer();
app.Plugin.register(new WebsocketAppAuthPlugin());
app.Plugin.register(new AuthPlugin());
app.Plugin.register(new ChatPlugin());
app.start();