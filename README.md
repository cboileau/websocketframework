#WebsocketServerTS

![version](https://img.shields.io/badge/version-0.2.0-red.svg)

> ## Examples

> There is a sample server implementation in [./src/SampleApp](./src/SampleApp/)

## Features

- Handle incoming websocket connections
- Route incoming requests to services

