"use strict";
/// <reference path='../../typings/modules/ws/index.d.ts' />
const ws = require("ws");
const ResponseManager_1 = require("./Response/ResponseManager");
const RequestManager_1 = require("./Request/RequestManager");
const PluginManager_1 = require("./Plugin/PluginManager");
const RouteManager_1 = require("./Routing/RouteManager");
const TransportManager_1 = require("./Transport/TransportManager");
class WebsocketAppServer {
    constructor(options) {
        this.transportManager = new TransportManager_1.TransportManager(this);
        this.responseManager = new ResponseManager_1.ResponseManager(this);
        this.requestManager = new RequestManager_1.RequestManager(this);
        this.pluginManager = new PluginManager_1.PluginManager(this);
        this.routeManager = new RouteManager_1.RouteManager(this);
        this.options = new WebsocketAppOptions(options);
    }
    get Request() {
        return this.requestManager;
    }
    get Response() {
        return this.responseManager;
    }
    get Plugin() {
        return this.pluginManager;
    }
    get Route() {
        return this.routeManager;
    }
    get Transport() {
        return this.transportManager;
    }
    start() {
        console.log("Starting WebsocketAppServer");
        console.log("Starting Connection and Request Monitoring");
        this.Request.monitorRequestStream();
        console.log("RequestManager Monitoring Request Stream");
        this.Transport.monitorConnectionStream();
        console.log("TransportManager Monitoring Connection Stream");
        this.Transport.monitorRequestStream();
        console.log("TransportManager Monitoring Request Stream");
        console.log("WebsocketAppServer is running");
    }
}
exports.WebsocketAppServer = WebsocketAppServer;
class WebsocketAppOptions {
    constructor(options) {
        this.defaults = {
            transportType: "websocket"
        };
        this.transportType = this.defaults.transportType;
        if (options) {
            if (options.hasOwnProperty("transportType")) {
                this.transportType = options.transportType;
            }
        }
    }
}
exports.WebsocketAppOptions = WebsocketAppOptions;
//# sourceMappingURL=WebsocketAppServer.js.map