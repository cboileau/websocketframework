import { WebsocketAppPlugin } from "../../Plugin/WebsocketAppPlugin";
import { WebsocketAppServer } from "../../WebsocketAppServer";
export declare class ValidationPlugin extends WebsocketAppPlugin {
    pluginName: string;
    registerPlugin(server: WebsocketAppServer): void;
}
