import { ValidatorInterface, RuleResult } from "../Rule";
export declare class StringValidator implements ValidatorInterface {
    validate(input: any): RuleResult;
}
