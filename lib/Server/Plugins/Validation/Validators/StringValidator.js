"use strict";
const Rule_1 = require("../Rule");
class StringValidator {
    validate(input) {
        if (typeof input == "string") {
            return new Rule_1.RuleResult(true);
        }
        else {
            return new Rule_1.RuleResult(false, "Input is not a string");
        }
    }
}
exports.StringValidator = StringValidator;
//# sourceMappingURL=StringValidator.js.map