"use strict";
const StringValidator_1 = require("./Validators/StringValidator");
class Rule {
    constructor() {
        this.validations = [];
    }
    string() {
        this.validations.push(new StringValidator_1.StringValidator());
    }
    validate() {
        for (let validation of this.validations) {
            const result = validation.validate();
            if (!result.passed()) {
                console.error(result.error());
                return false;
            }
        }
        return true;
    }
}
exports.Rule = Rule;
class RuleResult {
    constructor(success, errorMessage = "") {
        this.success = success;
        this.success = success;
    }
    passed() {
        return this.success;
    }
    error() {
        return this.errorMessage;
    }
}
exports.RuleResult = RuleResult;
//# sourceMappingURL=Rule.js.map