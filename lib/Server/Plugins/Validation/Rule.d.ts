export declare class Rule {
    private validations;
    string(): void;
    validate(): boolean;
}
export declare class RuleResult {
    private success;
    private errorMessage;
    constructor(success: boolean, errorMessage?: string);
    passed(): boolean;
    error(): string;
}
export interface ValidatorInterface {
    validate: (input) => RuleResult;
}
