"use strict";
const WebsocketAppPlugin_1 = require("../../Plugin/WebsocketAppPlugin");
class WebsocketAppAuthPlugin extends WebsocketAppPlugin_1.WebsocketAppPlugin {
    constructor() {
        super(...arguments);
        this.pluginName = "WSA.Auth";
    }
    registerPlugin(server) {
        console.log("Registing Auth Plugin");
        server.Plugin.registerConnectionHook(this);
        server.Plugin.registerRawRequestHook(this);
    }
    onConnection(connection) {
        console.log("AuthPlugin OnConnection Hook");
        connection.auth = true;
    }
    onRequest(request) {
        if (request.getServiceHandlerConfig().auth === true) {
            if (request.connection.auth !== true) {
                console.log("Unauthorized Access");
                request.unauthorized();
            }
        }
    }
}
exports.WebsocketAppAuthPlugin = WebsocketAppAuthPlugin;
//# sourceMappingURL=AuthPlugin.js.map