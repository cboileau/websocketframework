import { WebsocketAppPlugin } from "../../Plugin/WebsocketAppPlugin";
import { WebsocketAppServer } from "../../WebsocketAppServer";
import { IConnection } from "../../Transport/Connection/IConnection";
import { ClientRequest } from "../../Request/ClientRequest";
export declare class WebsocketAppAuthPlugin extends WebsocketAppPlugin {
    pluginName: string;
    registerPlugin(server: WebsocketAppServer): void;
    onConnection(connection: IConnection): void;
    onRequest(request: ClientRequest): void;
}
