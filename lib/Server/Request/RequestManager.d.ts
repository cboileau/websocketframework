import { WebsocketAppServer } from "../WebsocketAppServer";
import { Subject } from "@reactivex/rxjs";
import { ClientRequest } from "./ClientRequest";
export declare class RequestManager {
    server: WebsocketAppServer;
    rawRequestStream: Subject<ClientRequest>;
    processedRequestStream: Subject<ClientRequest>;
    rawRequestHooks: Array<any>;
    constructor(server: WebsocketAppServer);
    monitorRequestStream(): void;
    processRawRequestHooks(request: ClientRequest): void;
}
