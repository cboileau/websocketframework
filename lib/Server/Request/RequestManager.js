"use strict";
const rxjs_1 = require("@reactivex/rxjs");
class RequestManager {
    constructor(server) {
        this.server = server;
        this.rawRequestStream = new rxjs_1.Subject();
        this.processedRequestStream = new rxjs_1.Subject();
    }
    monitorRequestStream() {
        this.server.Transport.requestStream.subscribe({
            next: (request) => {
                this.rawRequestStream.next(request);
                this.processRawRequestHooks(request);
            }
        });
    }
    processRawRequestHooks(request) {
        for (let requestHook of this.server.Plugin.requestHooks) {
            try {
                requestHook.onRequest(request);
            }
            catch (error) {
                console.error("Error processing request hook");
                console.error(error);
            }
        }
        request.processed();
        this.processedRequestStream.next(request);
    }
}
exports.RequestManager = RequestManager;
//# sourceMappingURL=RequestManager.js.map