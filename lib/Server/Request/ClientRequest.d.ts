import { ServerResponse, ServerResponseAbstract } from "../Response/ServerResponse";
import { IConnection } from "../Transport/Connection/IConnection";
import { IServiceHandlerConfig } from "../Services/IServiceHandlerConfig";
export interface ClientRequestInterface {
    service: string;
    payload?: any | null;
    requestId?: string;
}
export declare class ClientRequest {
    connection: IConnection;
    static STATE_NEW: number;
    static STATE_PROCESSED: number;
    static STATE_COMPLETE: number;
    static STATE_FAILED: number;
    service: string;
    payload?: any | null;
    requestId?: string;
    private state;
    constructor(connection: IConnection);
    respond(responseData: ServerResponseAbstract): ServerResponse;
    response(): ServerResponse;
    unauthorized(): ServerResponse;
    getServiceHandlerConfig(): IServiceHandlerConfig;
    processed(): void;
    shouldRoute(): boolean;
}
