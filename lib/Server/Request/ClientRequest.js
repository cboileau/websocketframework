"use strict";
const ServerResponse_1 = require("../Response/ServerResponse");
const ErrorResponse_1 = require("../Response/ErrorResponse");
class ClientRequest {
    constructor(connection) {
        this.connection = connection;
        this.state = ClientRequest.STATE_NEW;
    }
    respond(responseData) {
        let response = new ServerResponse_1.ServerResponse(this.connection).loadAbstract(responseData);
        response.requestId = this.requestId;
        return response.send();
    }
    response() {
        let response = new ServerResponse_1.ServerResponse(this.connection);
        response.requestId = this.requestId;
        return response;
    }
    unauthorized() {
        this.state = ClientRequest.STATE_COMPLETE;
        let response = new ServerResponse_1.ServerResponse(this.connection).loadAbstract(new ErrorResponse_1.ErrorResponse("Unauthorized", 401));
        response.requestId = this.requestId;
        return response.send();
    }
    getServiceHandlerConfig() {
        return this.connection.server.Route.getServiceHandlerConfig(this.service);
    }
    processed() {
        this.state = ClientRequest.STATE_PROCESSED;
    }
    shouldRoute() {
        if (this.state === ClientRequest.STATE_COMPLETE || this.state === ClientRequest.STATE_FAILED) {
            return false;
        }
        return true;
    }
}
ClientRequest.STATE_NEW = 0;
ClientRequest.STATE_PROCESSED = 1;
ClientRequest.STATE_COMPLETE = 10;
ClientRequest.STATE_FAILED = 20;
exports.ClientRequest = ClientRequest;
//# sourceMappingURL=ClientRequest.js.map