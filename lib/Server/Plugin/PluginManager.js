"use strict";
class PluginManager {
    constructor(server) {
        this.server = server;
        this.requestHooks = [];
        this.connectionHooks = [];
        this.plugins = new Map();
    }
    register(plugin) {
        try {
            plugin.registerPlugin(this.server);
            this.plugins.set(plugin.getName(), plugin);
            console.log("Registered Plugin: " + plugin.getName());
        }
        catch (error) {
            console.error("Error registering plugin");
            console.error(error);
        }
    }
    registerRawRequestHook(plugin) {
        this.requestHooks.push(plugin);
    }
    registerConnectionHook(plugin) {
        this.connectionHooks.push(plugin);
    }
}
exports.PluginManager = PluginManager;
//# sourceMappingURL=PluginManager.js.map