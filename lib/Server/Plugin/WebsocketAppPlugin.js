"use strict";
class WebsocketAppPlugin {
    getName() {
        return this.pluginName;
    }
    onConnection(connection) { }
    onRequest(request) { }
}
exports.WebsocketAppPlugin = WebsocketAppPlugin;
//# sourceMappingURL=WebsocketAppPlugin.js.map