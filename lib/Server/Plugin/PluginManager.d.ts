import { WebsocketAppServer } from "../WebsocketAppServer";
import { WebsocketAppPlugin } from "./WebsocketAppPlugin";
export declare class PluginManager {
    private server;
    requestHooks: Array<WebsocketAppPlugin>;
    connectionHooks: Array<WebsocketAppPlugin>;
    plugins: Map<string, WebsocketAppPlugin>;
    constructor(server: WebsocketAppServer);
    register(plugin: WebsocketAppPlugin): void;
    registerRawRequestHook(plugin: WebsocketAppPlugin): void;
    registerConnectionHook(plugin: WebsocketAppPlugin): void;
}
