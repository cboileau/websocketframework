import { WebsocketAppServer } from "../WebsocketAppServer";
import { IConnection } from "../Transport/Connection/IConnection";
import { ClientRequest } from "../Request/ClientRequest";
export declare abstract class WebsocketAppPlugin {
    abstract pluginName: string;
    abstract registerPlugin(server: WebsocketAppServer): void;
    getName(): string;
    onConnection(connection: IConnection): void;
    onRequest(request: ClientRequest): void;
}
