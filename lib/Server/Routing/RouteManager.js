"use strict";
/**
 * Manages service routes and routes ClientRequests to service handlers
 */
class RouteManager {
    constructor(server) {
        this.server = server;
        this.routes = new Map();
        this.monitorProcessedRequestStream();
    }
    /**
     * Registers and array of route configrations
     * @param routeConfigs
     */
    registerRoutes(routeConfigs) {
        for (let routeConfig of routeConfigs) {
            this.register(routeConfig);
        }
    }
    getRouteConfig(path) {
        if (this.routes.has(path)) {
            return this.routes.get(path);
        }
        console.error("Route with path <" + path + "> is not registered");
    }
    getServiceHandlerConfig(path) {
        if (this.routes.has(path)) {
            return this.routes.get(path).handler;
        }
        console.error("Route with path <" + path + "> is not registered");
    }
    /**
     * Monitors processedRequestStream and triggers routing the request
     */
    monitorProcessedRequestStream() {
        this.server.Request.processedRequestStream.subscribe({
            next: (clientRequest) => {
                this.routeRequest(clientRequest);
            }
        });
    }
    /**
     * Routes a ClientRequest to route handler with matching service path
     * @param request
     * @returns {ServerResponse}
     */
    routeRequest(request) {
        if (this.routes.has(request.service)) {
            if (request.shouldRoute()) {
                console.log("Routing Request: " + request.service);
                this.routes.get(request.service).handler.handler(request);
            }
        }
        else {
            console.error("Cannot route request. Route with path <" + request.service + "> is not registered");
        }
    }
    /**
     * Registers a route configration
     * @param routeConfig
     */
    register(routeConfig) {
        if (this.routes.has(routeConfig.path)) {
            throw new Error("Service path: <" + routeConfig.path + "> has already been registered");
        }
        this.routes.set(routeConfig.path, routeConfig);
    }
}
exports.RouteManager = RouteManager;
//# sourceMappingURL=RouteManager.js.map