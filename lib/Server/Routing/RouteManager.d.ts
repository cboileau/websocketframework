import { WebsocketAppServer } from "../WebsocketAppServer";
import { IRouteConfig } from "../Services/IRouteConfig";
import { ClientRequest } from "../Request/ClientRequest";
import { IServiceHandlerConfig } from "../Services/IServiceHandlerConfig";
/**
 * Manages service routes and routes ClientRequests to service handlers
 */
export declare class RouteManager {
    server: WebsocketAppServer;
    private routes;
    constructor(server: WebsocketAppServer);
    /**
     * Registers and array of route configrations
     * @param routeConfigs
     */
    registerRoutes(routeConfigs: Array<IRouteConfig>): void;
    getRouteConfig(path: string): IRouteConfig;
    getServiceHandlerConfig(path: string): IServiceHandlerConfig;
    /**
     * Monitors processedRequestStream and triggers routing the request
     */
    monitorProcessedRequestStream(): void;
    /**
     * Routes a ClientRequest to route handler with matching service path
     * @param request
     * @returns {ServerResponse}
     */
    routeRequest(request: ClientRequest): void;
    /**
     * Registers a route configration
     * @param routeConfig
     */
    register(routeConfig: IRouteConfig): void;
}
