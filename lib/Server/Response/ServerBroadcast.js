"use strict";
class ServerBroadcast {
    constructor(server) {
        this.server = server;
        this.statusCode = 200;
        this._connections = [];
    }
    connections(connections) {
        this._connections = connections;
    }
    sendAll() {
        let connections = this.server.Transport.getAllConnections().toArray();
        this.connections(connections);
        this.send();
    }
    send() {
        for (let connection of this._connections) {
            connection.sendBroadcast(this);
        }
    }
}
exports.ServerBroadcast = ServerBroadcast;
//# sourceMappingURL=ServerBroadcast.js.map