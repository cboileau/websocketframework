"use strict";
const ServerResponse_1 = require("./ServerResponse");
class ErrorResponse extends ServerResponse_1.ServerResponseAbstract {
    constructor(message, statusCode) {
        super();
        this.payload = {
            errorMessage: message
        };
        this.statusCode = statusCode;
    }
}
exports.ErrorResponse = ErrorResponse;
//# sourceMappingURL=ErrorResponse.js.map