"use strict";
const _ = require("lodash");
const ServerBroadcast_1 = require("./ServerBroadcast");
const Immutable = require("immutable");
class ResponseManager {
    constructor(server) {
        this.server = server;
        this.responses = Immutable.Map();
    }
    createBroadcast() {
        return new ServerBroadcast_1.ServerBroadcast(this.server);
    }
    register(key, serverResponse) {
        if (this.responses.has(key)) {
            throw new Error("A response with the name <" + key + "> has already been registered");
        }
        else {
            this.responses.set(key, serverResponse);
        }
    }
    /**
     * Returns a clone of the response type
     */
    get(key) {
        if (this.responses.has(key)) {
            return _.clone(this.responses.get(key));
        }
    }
}
exports.ResponseManager = ResponseManager;
//# sourceMappingURL=ResponseManager.js.map