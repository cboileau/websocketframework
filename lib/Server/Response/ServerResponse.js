"use strict";
class ServerResponseAbstract {
    constructor() {
        this.statusCode = 200;
    }
    status(statusCode) {
        this.statusCode = statusCode;
    }
    get(key) {
        if (this.hasOwnProperty(key)) {
            return this[key];
        }
        return undefined;
    }
    has(key) {
        return this.hasOwnProperty(key);
    }
}
exports.ServerResponseAbstract = ServerResponseAbstract;
class ServerResponse {
    constructor(connection) {
        this.connection = connection;
        this.statusCode = 200;
        this.server = this.connection.server;
    }
    loadAbstract(abstract) {
        if (abstract.has("statusCode"))
            this.statusCode = abstract.get("statusCode");
        if (abstract.has("payload"))
            this.payload = abstract.get("payload");
        if (abstract.has("payloadClass"))
            this.payloadClass = abstract.get("payloadClass");
        if (abstract.has("event"))
            this.event = abstract.get("event");
        return this;
    }
    loadAttributes(responseAttributes) {
        this.set(responseAttributes, [
            "payload",
            "payloadClass",
            "event",
            "statusCode"
        ]);
    }
    set(attributes, props) {
        for (let prop of props) {
            if (attributes.hasOwnProperty(prop)) {
                this[prop] = attributes[prop];
            }
        }
    }
    toObject() {
        return {
            requestId: this.requestId,
            statusCode: this.statusCode,
            payload: this.payload,
            payloadClass: this.payloadClass,
            event: this.event
        };
    }
    toJSON() {
        return JSON.stringify(this.toObject());
    }
    send() {
        try {
            console.log("Responding to client: ");
            console.log(this.toObject());
            this.connection.sendResponse(this);
        }
        catch (error) {
            console.error(error);
        }
        return this;
    }
    to(requestId) {
        this.requestId = requestId;
        return this;
    }
    status(statusCode) {
        this.statusCode = statusCode;
        return this;
    }
    with(payload) {
        this.payload = payload;
        return this;
    }
    asClass(payloadClass) {
        this.payloadClass = payloadClass;
        return this;
    }
    triggering(event) {
        this.event = event;
        return this;
    }
}
exports.ServerResponse = ServerResponse;
//# sourceMappingURL=ServerResponse.js.map