import { ServerResponseAbstract } from "./ServerResponse";
export declare class ErrorResponse extends ServerResponseAbstract {
    constructor(message: string, statusCode: number);
}
