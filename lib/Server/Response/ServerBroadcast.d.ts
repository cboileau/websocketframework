import { WebsocketAppServer } from "../WebsocketAppServer";
import { IConnection } from "../Transport/Connection/IConnection";
export declare class ServerBroadcast {
    server: WebsocketAppServer;
    statusCode: number;
    payload: any;
    payloadClass: string | null;
    event: string;
    private _connections;
    connections(connections: Array<IConnection>): void;
    sendAll(): void;
    send(): void;
    constructor(server: WebsocketAppServer);
}
