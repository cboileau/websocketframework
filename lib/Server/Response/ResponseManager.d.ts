import { ServerResponseInterface } from "./ServerResponse";
import { WebsocketAppServer } from "../WebsocketAppServer";
import { ServerBroadcast } from "./ServerBroadcast";
export declare class ResponseManager {
    server: WebsocketAppServer;
    private responses;
    constructor(server: WebsocketAppServer);
    createBroadcast(): ServerBroadcast;
    register(key: any, serverResponse: ServerResponseInterface): void;
    /**
     * Returns a clone of the response type
     */
    get(key: any): ServerResponseInterface;
}
