import { WebsocketAppServer } from "../WebsocketAppServer";
import { IConnection } from "../Transport/Connection/IConnection";
export declare abstract class ServerResponseAbstract {
    statusCode: number;
    payload: any;
    payloadClass: string | null;
    event: string;
    status(statusCode: number): void;
    get(key: any): any;
    has(key: any): boolean;
}
export interface ServerResponseInterface {
    statusCode?: number;
    payload?: any;
    payloadClass?: string | null;
    event?: string;
}
export declare class ServerResponse {
    connection: IConnection;
    server: WebsocketAppServer;
    requestId: string | null;
    statusCode: number;
    payload: any;
    payloadClass: string | null;
    event: string;
    constructor(connection: IConnection);
    loadAbstract(abstract: ServerResponseAbstract): this;
    loadAttributes(responseAttributes: ServerResponseInterface): void;
    private set(attributes, props);
    toObject(): {
        requestId: string;
        statusCode: number;
        payload: any;
        payloadClass: string;
        event: string;
    };
    toJSON(): string;
    send(): this;
    to(requestId: string): this;
    status(statusCode: number): this;
    with(payload: any): this;
    asClass(payloadClass: string): this;
    triggering(event: string): this;
}
