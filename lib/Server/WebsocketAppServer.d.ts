/// <reference path="../../typings/modules/ws/index.d.ts" />
import { ResponseManager } from "./Response/ResponseManager";
import { RequestManager } from "./Request/RequestManager";
import { PluginManager } from "./Plugin/PluginManager";
import { RouteManager } from "./Routing/RouteManager";
import { TransportManager } from "./Transport/TransportManager";
export declare class WebsocketAppServer {
    private options;
    private transportManager;
    private responseManager;
    private requestManager;
    private pluginManager;
    private routeManager;
    constructor(options?: any);
    readonly Request: RequestManager;
    readonly Response: ResponseManager;
    readonly Plugin: PluginManager;
    readonly Route: RouteManager;
    readonly Transport: TransportManager;
    start(): void;
}
export interface WebsocketAppOptionsAttributes {
    transportType?: string;
}
export declare class WebsocketAppOptions {
    transportType: string;
    defaults: {
        transportType: string;
    };
    constructor(options?: WebsocketAppOptionsAttributes);
}
