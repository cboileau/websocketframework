import { WebsocketAppServer } from "../WebsocketAppServer";
import { IConnection } from "./Connection/IConnection";
import { Subject } from "@reactivex/rxjs";
import { ClientRequest } from "../Request/ClientRequest";
import Immutable = require("immutable");
/**
 * Manages the data transport layer of the server
 */
export declare class TransportManager {
    private server;
    rawConnectionStream: Subject<IConnection>;
    processedConnectionStream: Subject<IConnection>;
    requestStream: Subject<ClientRequest>;
    private transportInterface;
    constructor(server: WebsocketAppServer);
    /**
     * Sets transportInterface to an implementation of ITransport based on server Config
     */
    private setTransportImplementation();
    getAllConnections(): Immutable.Map<string, IConnection>;
    /**
     * Monitors transportInterfaces raw ConnectionInterfaces, updates rawConnectionInterface, and triggers processing raw connection hooks
     */
    monitorConnectionStream(): void;
    /**
     * Monitors transportInterfaces raw ClientRequests and updates requestStream
     */
    monitorRequestStream(): void;
    /**
     * Processes connections hooks on raw ConnectionInterfaces and updates processConnectionStream
     * @param connection
     */
    private processRawConnectionHooks(connection);
}
