import Immutable = require("immutable");
import { IConnection } from "./Connection/IConnection";
import { ServerResponse } from "../Response/ServerResponse";
import { ClientRequest } from "../Request/ClientRequest";
import { Subject } from "@reactivex/rxjs";
export interface ITransport {
    connections: Immutable.Map<string, IConnection>;
    connectionStream: Subject<IConnection>;
    requestStream: Subject<ClientRequest>;
    close(): Promise<boolean>;
    send(response: ServerResponse): Promise<any>;
}
