"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const Immutable = require("immutable");
const WebsocketConnection_1 = require("./WebsocketConnection");
const rxjs_1 = require("@reactivex/rxjs");
const ws = require("ws");
class WebsocketTransport {
    constructor(server) {
        this.server = server;
        this.connections = Immutable.Map();
        this.connectionStream = new rxjs_1.Subject();
        this.requestStream = new rxjs_1.Subject();
        this.wss = new ws.Server({ port: 3000 });
        console.log(this.wss);
        this._bindHandlers();
        this._subscribeEvents();
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.closeServer();
                return true;
            }
            catch (error) {
                console.error(error);
                return false;
            }
        });
    }
    send(response) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
            }
            catch (error) {
            }
        });
    }
    _subscribeEvents() {
        this.monitorConnections();
    }
    monitorConnections() {
        this.connectionStream.subscribe({
            next: (connection) => {
                connection.requestStream.subscribe({
                    next: (clientRequest) => {
                        this.requestStream.next(clientRequest);
                    }
                });
                connection.status.subscribe({
                    complete: () => {
                        this.connections.delete(connection.uuid);
                    }
                });
            }
        });
    }
    _bindHandlers() {
        console.log("Binding Websocket Server event handlers");
        const WebsocketTransport = this;
        this.wss.on("connection", (ws) => {
            WebsocketTransport.onConnection(ws);
        });
    }
    onConnection(clientConnection) {
        let connection = new WebsocketConnection_1.WebsocketConnection(this.server, clientConnection);
        console.log("New client connected (" + connection.uuid + ")");
        this.connections.set(connection.uuid, connection);
        this.connectionStream.next(connection);
    }
    closeServer() {
        return new Promise((resolve, reject) => {
            try {
                this.wss.close();
                resolve(true);
            }
            catch (error) {
                reject(error);
            }
        });
    }
}
exports.WebsocketTransport = WebsocketTransport;
//# sourceMappingURL=WebsocketTransport.js.map