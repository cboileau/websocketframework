import Immutable = require("immutable");
import { WebsocketConnection } from "./WebsocketConnection";
import { ITransport } from "../../ITransport";
import { IConnection } from "../../Connection/IConnection";
import { ServerResponse } from "../../../Response/ServerResponse";
import { WebsocketAppServer } from "../../../WebsocketAppServer";
import { Subject } from "@reactivex/rxjs";
import { ClientRequest } from "../../../Request/ClientRequest";
export declare class WebsocketTransport implements ITransport {
    private server;
    private wss;
    connections: Immutable.Map<string, IConnection>;
    connectionStream: Subject<WebsocketConnection>;
    requestStream: Subject<ClientRequest>;
    constructor(server: WebsocketAppServer);
    close(): Promise<boolean>;
    send(response: ServerResponse): Promise<any>;
    private _subscribeEvents();
    private monitorConnections();
    private _bindHandlers();
    onConnection(clientConnection: any): void;
    closeServer(): Promise<boolean>;
}
