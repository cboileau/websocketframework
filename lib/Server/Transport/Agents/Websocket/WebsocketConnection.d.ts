import { IConnection } from "../../Connection/IConnection";
import { ClientRequest } from "../../../Request/ClientRequest";
import { WebsocketAppServer } from "../../../WebsocketAppServer";
import { ServerResponse } from "../../../Response/ServerResponse";
import { Subject } from "@reactivex/rxjs";
import { ServerBroadcast } from "../../../Response/ServerBroadcast";
export declare class WebsocketConnection implements IConnection {
    server: WebsocketAppServer;
    private websocket;
    uuid: string;
    requestStream: Subject<ClientRequest>;
    status: Subject<string>;
    constructor(server: WebsocketAppServer, websocket: any);
    onClose(): void;
    onRequest(requestString: string): void;
    sendResponse(serverResponse: ServerResponse): void;
    sendBroadcast(serverBroadcast: ServerBroadcast): void;
}
