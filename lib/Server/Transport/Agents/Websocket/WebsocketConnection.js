"use strict";
const ClientRequest_1 = require("../../../Request/ClientRequest");
const rxjs_1 = require("@reactivex/rxjs");
const uuid = require("node-uuid");
class WebsocketConnection {
    constructor(server, websocket) {
        this.server = server;
        this.websocket = websocket;
        this.requestStream = new rxjs_1.Subject();
        this.status = new rxjs_1.Subject();
        this.uuid = uuid.v4();
        const connection = this;
        this.websocket.on("message", (requestString) => {
            connection.onRequest(requestString);
        });
        this.websocket.on("close", () => {
            connection.onClose();
        });
        this.status.next("open");
    }
    onClose() {
        this.status.next("closing");
        this.status.next("closed");
        this.status.complete();
    }
    onRequest(requestString) {
        const requestObj = JSON.parse(requestString);
        const clientRequest = new ClientRequest_1.ClientRequest(this);
        clientRequest.connection = this;
        clientRequest.payload = requestObj.payload;
        clientRequest.requestId = requestObj.requestId;
        clientRequest.service = requestObj.service;
        this.requestStream.next(clientRequest);
    }
    sendResponse(serverResponse) {
        this.websocket.send(serverResponse.toJSON());
    }
    sendBroadcast(serverBroadcast) {
        this.websocket.send(serverBroadcast);
    }
}
exports.WebsocketConnection = WebsocketConnection;
//# sourceMappingURL=WebsocketConnection.js.map