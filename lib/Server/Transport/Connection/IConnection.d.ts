import { WebsocketAppServer } from "../../WebsocketAppServer";
import { ServerResponse } from "../../Response/ServerResponse";
import { Observable } from "@reactivex/rxjs";
import { ClientRequest } from "../../Request/ClientRequest";
import { ServerBroadcast } from "../../Response/ServerBroadcast";
export interface IConnection {
    auth?: boolean;
    server: WebsocketAppServer;
    uuid: string;
    requestStream: Observable<ClientRequest>;
    sendResponse(serverResponse: ServerResponse): any;
    sendBroadcast(serverBroadcast: ServerBroadcast): any;
    onRequest(requestString: string): any;
}
