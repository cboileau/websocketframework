"use strict";
const rxjs_1 = require("@reactivex/rxjs");
const WebsocketTransport_1 = require("./Agents/Websocket/WebsocketTransport");
/**
 * Manages the data transport layer of the server
 */
class TransportManager {
    constructor(server) {
        this.server = server;
        this.rawConnectionStream = new rxjs_1.Subject();
        this.processedConnectionStream = new rxjs_1.Subject();
        this.requestStream = new rxjs_1.Subject();
        this.setTransportImplementation();
    }
    /**
     * Sets transportInterface to an implementation of ITransport based on server Config
     */
    setTransportImplementation() {
        // TODO read config to switch between implementations
        this.transportInterface = new WebsocketTransport_1.WebsocketTransport(this.server);
    }
    getAllConnections() {
        return this.transportInterface.connections;
    }
    /**
     * Monitors transportInterfaces raw ConnectionInterfaces, updates rawConnectionInterface, and triggers processing raw connection hooks
     */
    monitorConnectionStream() {
        this.transportInterface.connectionStream.subscribe({
            next: (connection) => {
                this.rawConnectionStream.next(connection);
                this.processRawConnectionHooks(connection);
            }
        });
    }
    /**
     * Monitors transportInterfaces raw ClientRequests and updates requestStream
     */
    monitorRequestStream() {
        this.transportInterface.requestStream.subscribe({
            next: (request) => {
                this.requestStream.next(request);
            }
        });
    }
    /**
     * Processes connections hooks on raw ConnectionInterfaces and updates processConnectionStream
     * @param connection
     */
    processRawConnectionHooks(connection) {
        for (let connectionHook of this.server.Plugin.connectionHooks) {
            try {
                connectionHook.onConnection(connection);
            }
            catch (error) {
                console.error("Error processing connection hook");
                console.error(error);
            }
        }
        this.processedConnectionStream.next(connection);
    }
}
exports.TransportManager = TransportManager;
//# sourceMappingURL=TransportManager.js.map