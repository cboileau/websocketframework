"use strict";
class ServiceManager {
    constructor(server) {
        this.server = server;
        this.services = new Map();
    }
    register(path, endpointConfig) {
        if (this.services.has(endpointConfig.path)) {
            throw new Error("Service path: <" + endpointConfig.path + "> has already been registered");
        }
        this.services.set(endpointConfig.path, endpointConfig);
    }
}
exports.ServiceManager = ServiceManager;
//# sourceMappingURL=ServiceManager.js.map