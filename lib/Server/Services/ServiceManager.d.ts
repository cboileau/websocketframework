import { WebsocketAppServer } from "../WebsocketAppServer";
import { IRouteConfig } from "./IRouteConfig";
export declare class ServiceManager {
    server: WebsocketAppServer;
    private services;
    constructor(server: WebsocketAppServer);
    register(path: string, endpointConfig: IRouteConfig): void;
}
