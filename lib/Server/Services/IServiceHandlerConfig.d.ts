import { ClientRequest } from "../Request/ClientRequest";
export interface IServiceHandlerConfig {
    handler: (request: ClientRequest) => void;
    auth?: boolean;
}
