"use strict";
const WebsocketAppPlugin_1 = require("../../../Server/Plugin/WebsocketAppPlugin");
const ChatService_1 = require("./ChatService");
class ChatPlugin extends WebsocketAppPlugin_1.WebsocketAppPlugin {
    constructor() {
        super(...arguments);
        this.pluginName = "ChatPlugin";
    }
    registerPlugin(server) {
        let chatService = new ChatService_1.ChatService();
        server.Route.registerRoutes([
            { path: "chat/join", handler: chatService.joinChat() },
            { path: "chat/addMessage", handler: chatService.addMessage() }
        ]);
    }
}
exports.ChatPlugin = ChatPlugin;
//# sourceMappingURL=ChatPlugin.js.map