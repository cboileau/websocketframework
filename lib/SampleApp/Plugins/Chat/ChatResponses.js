"use strict";
const ServerResponse_1 = require("../../../Server/Response/ServerResponse");
class ChatMessageResponse extends ServerResponse_1.ServerResponseAbstract {
    constructor() {
        super(...arguments);
        this.payload = { message: undefined };
    }
    message(message) {
        this.payload.message = message;
        return this;
    }
}
exports.ChatMessageResponse = ChatMessageResponse;
class ChatMessageSentResponse extends ServerResponse_1.ServerResponseAbstract {
    constructor() {
        super(...arguments);
        this.statusCode = 200;
    }
}
exports.ChatMessageSentResponse = ChatMessageSentResponse;
//# sourceMappingURL=ChatResponses.js.map