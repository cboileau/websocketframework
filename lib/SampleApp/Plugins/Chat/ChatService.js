"use strict";
const rxjs_1 = require("@reactivex/rxjs");
const ChatResponses_1 = require("./ChatResponses");
class Message {
    constructor(username, message) {
        this.username = username;
        this.message = message;
    }
}
exports.Message = Message;
class ChatService {
    constructor() {
        this.messages = new rxjs_1.ReplaySubject(30);
    }
    joinChat() {
        return {
            auth: true,
            handler: (request) => {
                this.messages.next(new Message(request.connection.uuid, "has joined chat!"));
                this.messages.subscribe({
                    next: (message) => {
                        request.respond(new ChatResponses_1.ChatMessageResponse().message(message));
                    }
                });
            }
        };
    }
    addMessage() {
        return {
            auth: true,
            handler: (request) => {
                console.log();
                this.messages.next(new Message(request.connection.uuid, request.payload.message));
                request.respond(new ChatResponses_1.ChatMessageSentResponse());
            }
        };
    }
}
exports.ChatService = ChatService;
//# sourceMappingURL=ChatService.js.map