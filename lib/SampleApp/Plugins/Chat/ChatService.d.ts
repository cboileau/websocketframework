import { ReplaySubject } from "@reactivex/rxjs";
import { ClientRequest } from "../../../Server/Request/ClientRequest";
export declare class Message {
    username: string;
    message: string;
    constructor(username: string, message: string);
}
export declare class ChatService {
    messages: ReplaySubject<Message>;
    joinChat(): {
        auth: boolean;
        handler: (request: ClientRequest) => void;
    };
    addMessage(): {
        auth: boolean;
        handler: (request: ClientRequest) => void;
    };
}
