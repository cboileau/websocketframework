import { ServerResponseAbstract } from "../../../Server/Response/ServerResponse";
import { Message } from "./ChatService";
export declare class ChatMessageResponse extends ServerResponseAbstract {
    payload: {
        message: Message;
    };
    message(message: Message): this;
}
export declare class ChatMessageSentResponse extends ServerResponseAbstract {
    payload: undefined;
    statusCode: number;
}
