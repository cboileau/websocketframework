"use strict";
const WebsocketAppPlugin_1 = require("../../../Server/Plugin/WebsocketAppPlugin");
const AuthService_1 = require("./AuthService");
class AuthPlugin extends WebsocketAppPlugin_1.WebsocketAppPlugin {
    constructor() {
        super(...arguments);
        this.pluginName = "AuthPlugin";
    }
    registerPlugin(server) {
        let authService = new AuthService_1.AuthService();
        server.Route.registerRoutes([
            { path: "user/auth", handler: authService.serviceEndpoint() },
        ]);
    }
}
exports.AuthPlugin = AuthPlugin;
//# sourceMappingURL=AuthPlugin.js.map