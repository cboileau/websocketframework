import { ClientRequest } from "../../../Server/Request/ClientRequest";
export declare class AuthService {
    serviceEndpoint(): {
        handler: (request: ClientRequest) => void;
    };
}
