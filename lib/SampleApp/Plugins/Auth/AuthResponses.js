"use strict";
const ServerResponse_1 = require("../../../Server/Response/ServerResponse");
class AuthResponse extends ServerResponse_1.ServerResponseAbstract {
    constructor() {
        super(...arguments);
        this.payload = {
            authSuccess: null,
            userId: undefined
        };
    }
    valid(userId) {
        this.statusCode = 200;
        this.payload.userId = userId;
        this.payload.authSuccess = true;
        return this;
    }
    invalid() {
        this.payload.authSuccess = false;
        return this;
    }
}
exports.AuthResponse = AuthResponse;
//# sourceMappingURL=AuthResponses.js.map