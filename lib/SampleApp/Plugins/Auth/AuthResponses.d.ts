import { ServerResponseAbstract } from "../../../Server/Response/ServerResponse";
export declare class AuthResponse extends ServerResponseAbstract {
    payload: {
        authSuccess: any;
        userId: any;
    };
    valid(userId: number): this;
    invalid(): this;
}
