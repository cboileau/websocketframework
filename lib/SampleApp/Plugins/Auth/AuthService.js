"use strict";
const AuthResponses_1 = require("./AuthResponses");
class AuthService {
    serviceEndpoint() {
        return {
            handler: (request) => {
                console.log("SERVICE ENDPOINT REACHED");
                if (request.payload.email === "craigboileau@gmail.com" && request.payload.password === "C1r9a8i7g") {
                    request.respond(new AuthResponses_1.AuthResponse().valid(1));
                }
                else {
                    request.respond(new AuthResponses_1.AuthResponse().invalid());
                }
            }
        };
    }
}
exports.AuthService = AuthService;
//# sourceMappingURL=AuthService.js.map