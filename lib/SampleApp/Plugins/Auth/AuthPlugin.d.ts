import { WebsocketAppPlugin } from "../../../Server/Plugin/WebsocketAppPlugin";
import { WebsocketAppServer } from "../../../Server/WebsocketAppServer";
export declare class AuthPlugin extends WebsocketAppPlugin {
    pluginName: string;
    registerPlugin(server: WebsocketAppServer): void;
}
