"use strict";
const WebsocketAppServer_1 = require("../Server/WebsocketAppServer");
const AuthPlugin_1 = require("./Plugins/Auth/AuthPlugin");
const AuthPlugin_2 = require("../Server/Plugins/Auth/AuthPlugin");
const ChatPlugin_1 = require("./Plugins/Chat/ChatPlugin");
const app = new WebsocketAppServer_1.WebsocketAppServer();
app.Plugin.register(new AuthPlugin_2.WebsocketAppAuthPlugin());
app.Plugin.register(new AuthPlugin_1.AuthPlugin());
app.Plugin.register(new ChatPlugin_1.ChatPlugin());
app.start();
//# sourceMappingURL=Server.js.map